package com.pabloillescas.ui;

import java.util.ArrayList;
import java.util.Scanner;
import com.pabloillescas.util.util;

public class main {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        ArrayList coches = new ArrayList();

        int cont;

        do {
            System.out.println("1.Añadir Coche "
                    + "\n2.Vender un Coche"
                    + "\n3.Alquilar un Coche"
                    + "\n4.Contador"
                    + "\n0.Salir");
            cont = teclado.nextInt();

            switch (cont) {
                case 1:
                    util.añadirCoche(coches);
                    System.out.println("----------");
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    util.contador(coches);
                    System.out.println("----------"); 
                    break;
                case 0:
                    System.out.println("Adios");
                    break;
            }
        } while (cont != 0);

    }
}
