package com.pabloillescas.util;

import java.util.ArrayList;
import java.util.Scanner;
import com.pabloillescas.entities.coche;


public class util {
    
    public static void añadirCoche(ArrayList coches){
        
        Scanner teclado= new Scanner(System.in);
        coche c;
        int id;
        String matricula, modelo;
        
        System.out.print("Id del coche: ");
        id=teclado.nextInt();
        
        System.out.print("Matricula del coche: ");
        teclado.next();
        matricula=teclado.nextLine();
        
        
        System.out.print("Modelo del coche: ");
        modelo=teclado.nextLine();
        
        c=new coche (id,matricula,modelo);
        coches.add(c);
    }
    
    public static void contador(ArrayList coches){
        int n=0;
        for (int i = 0; i < coches.size(); i++) {
            n++;
        }
        
        System.out.println("El concesionario tiene "+n);
    }
}
