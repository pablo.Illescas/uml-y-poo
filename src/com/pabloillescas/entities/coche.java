package com.pabloillescas.entities;

public class coche {
    private int id;
    private String matricula;
    private String modelo;

    public coche(int id, String matricula, String modelo) {
        this.id = id;
        this.matricula = matricula;
        this.modelo = modelo;
    }

    public int getId() {
        return id;
    }

    public String getMatricula() {
        return matricula;
    }

    public String getModelo() {
        return modelo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Override
    public String toString() {
        return "coche{" + "id=" + id + ", matricula=" + matricula + ", modelo=" + modelo + '}';
    }
    
    
}
